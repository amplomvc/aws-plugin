<?php

/**
 * Amazon Web Services Integration
 *
 * Version: 0.1
 * Title: AWS Integration w/ Amplo MVC
 * Description: Integrates Amazon web services functionality for Amplo MVC
 * Author: Daniel Newman
 * Date: 11/21/2014
 *
 */
class Plugin_Aws_Setup extends Plugin_Setup
{
	public function install()
	{
		message('notify', _l("In config.php, set any directory defines you would like stored in s3 to s3://my-bucket/..."));
	}

	public function uninstall($keep_data = false)
	{
		$config = file_get_contents(DIR_SITE . 'config.php');

		file_put_contents(DIR_SITE . 'config.php', preg_replace('#s3://[^\\\\/]/#', DIR_SITE, $config));

		message('notify', _l("Any s3 pointed directories in the config.php file have been restored to point to %s/your/custom/path/", DIR_SITE));
	}
}
