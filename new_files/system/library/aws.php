<?php

class Aws extends Library
{
	public function getPolicy($expires, $conditions = array(), $encode = true)
	{
		$policy = sprintf('{ "expiration": "%s"', gmdate('Y-n-d\TH:i:s.000\Z', time() + $expires));

		if (count($conditions)) {
			$policy .= ', "conditions": [';
			$first = true;
			foreach ($conditions as $condition) {
				if (!$first) {
					$policy .= ', ';
				}
				if (isset($condition[0])) {
					$policy .= sprintf('["%s", "%s", "%s"]', $condition[0], $condition[1], $condition[2]);
				} else {
					$policy .= sprintf('{"%s": "%s"}', key($condition), current($condition));
				}

				$first = false;
			}

			$policy .= ']';
		}

		$policy .= '}';

		if ($encode) {
			$policy = base64_encode(utf8_encode(preg_replace('/\s\s+|\\f|\\n|\\r|\\t|\\v/', '', $policy)));
		}

		return $policy;
	}
}
