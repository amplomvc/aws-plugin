<?php
if (defined('AWS_ACCESS_KEY') && is_dir(DIR_RESOURCES . 'Aws/')) {

	//Register early loader for AWS class loading.
	spl_autoload_register(function ($class) {
		$class = str_replace('\\', '/', $class);
		if (file_exists(DIR_RESOURCES . $class . '.php')) {
			require_once(DIR_RESOURCES . $class . '.php');
		}
	});

	//Allow usage of fopen(), and other stream wrapper functions to access AWS S3 bucket
	$client = Aws\S3\S3Client::factory(array(
		'key'    => AWS_ACCESS_KEY,
		'secret' => AWS_SECRET_KEY,
	));

	$client->registerStreamWrapper();
}
